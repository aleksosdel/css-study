//Создаём переменные
let project_folder = require("path").basename(__dirname);
let source_folder  = "src";

let fs = require('fs');
//Создаём переменные путей к файлам и папкам
let path = {
    build: {
        html: project_folder  + "/",
        css: project_folder   + "/css/",
        js: project_folder    + "/js/",
        img: project_folder   + "/img/",
        fonts: project_folder + "/fonts/",
    },
    src: {
        html: [source_folder + "/*.html", "!" + source_folder  + "/_*.html"],
        css: source_folder   + "/sass/style.sass",
        js: source_folder    + "/js/script.js",
        img: source_folder   + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
        fonts: source_folder + "/fonts/*.ttf",
        bs: source_folder   + "/css/bootstrap-grid.min.css",
    },
    watch: {
        html: source_folder + "/**/*.html",
        css: source_folder  + "/sass/**/*.sass",
        js: source_folder   + "/js/**/*.js",
        img: source_folder  + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    },
    //удаление папки при запуске
    clean: "./" + project_folder + "/"
}
//Создаём переменные для сценария
let { src, dest } = require('gulp'),
    gulp          = require('gulp'),
    browsersync   = require("browser-sync").create(),
    fileinclude   = require("gulp-file-include"),
    del           = require("del"),
    sass          = require("gulp-sass"),
    autoprefixer  = require("gulp-autoprefixer"),
    group_media   = require("gulp-group-css-media-queries"),
    clean_css     = require("gulp-clean-css"),
    rename        = require("gulp-rename"),
    uglify        = require("gulp-uglify-es").default,
    imagemin      = require("gulp-imagemin"),
    webp          = require("gulp-webp"),
    webphtml      = require("gulp-webp-html"),
    /*webpcss     = require("gulp-webpcss"),*/
    ttf2woff      = require("gulp-ttf2woff"),
    ttf2woff2     = require("gulp-ttf2woff2"),
    fonter        = require("gulp-fonter");
//Объявляум функцию browserSync
function browserSync(params) {
    browsersync.init({
        server: {
            baseDir: "./" + project_folder + "/"
        },
        port: 3000,
        notify: false
    })
}
//Объявляум функцию для работы html
function html() {
    return src(path.src.html)
        .pipe(fileinclude())
        .pipe(webphtml())
        .pipe(dest(path.build.html))
        .pipe(browsersync.stream())
}

//bootstrap
function bs() {
    return src(path.src.css)
        .pipe(dest(path.build.css))
        /*.pipe(browsersync.stream())*/
}
//Объявляум функцию для работы sass
function css() {
    return src(path.src.css)
        .pipe(
            sass({
                outputStyle: "expanded"
            })
        )
        .pipe(
            group_media()
        )
        .pipe(
            autoprefixer({
                grid: true,
                overrideBrowserslist: ["last 5 versions"],
                cascade: true
            })
        )
        /*.pipe(webpcss())*/
        .pipe(dest(path.build.css))
        .pipe(clean_css())
        .pipe(
            rename({
                extname: ".min.css"
            })
        )
        .pipe(dest(path.build.css))
        .pipe(browsersync.stream())
}
//Объявляум функцию для работы js
function js() {
    return src(path.src.js)
        .pipe(fileinclude())
        .pipe(dest(path.build.js))
        .pipe(
        		uglify()
        )
        .pipe(
            rename({
                extname: ".min.js"
            })
        )
        .pipe(dest(path.build.js))
        .pipe(browsersync.stream())
}
//Объявляум функцию для работы img
function images() {
    return src(path.src.img)
    		.pipe(
    				webp({
    						quality: 70
    				})
    		)
    		.pipe(dest(path.build.img))
    		.pipe(src(path.src.img))
    		.pipe(
    				imagemin({
    						progressive: true,
    						svgoPlugins: [{ removeViewBox: false }],
    						interlaced: true,
    						optimizationLevel: 3 // 0 to 7
    				})
    		)
        .pipe(dest(path.build.img))
        .pipe(browsersync.stream())
}

//Объявляем функцию обработки шрифтов
function fonts() {
		src(path.src.fonts)
				.pipe(ttf2woff())
				.pipe(dest(path.build.fonts));
		return src(path.src.fonts)
				.pipe(ttf2woff2())
				.pipe(dest(path.build.fonts));
}
//Объявляем функцию для шривтов otf
gulp.task('otf2ttf', function () {
		return src([source_folder + '/fonts/*.otf'])
				.pipe(fonter({
						formats: ['ttf']
				}))
				.pipe(dest(source_folder + '/fonts/'));
})

//Запись параметров в Font.sass
function fontsStyle(params) {
		let file_content = fs.readFileSync(source_folder + '/sass/fonts.sass');
		if (file_content == '') {
				fs.writeFile(source_folder + '/sass/fonts.sass', '', cb);
				return fs.readdir(path.build.fonts, function (err, items) {
						if (items) {
						let c_fontname;
								for (var i = 0; i < items.length; i++) {
										let fontname = items[i].split('.');
										fontname = fontname[0];
										if (c_fontname != fontname) {
												fs.appendFile(source_folder + '/sass/fonts.sass', '@include font("' + fontname + '", "' + fontname + '", "400", "normal")\r\n', cb);
										}
										c_fontname = fontname;
								}
						}
				})
		}
}

function cb() {

}

//Объявляем функцию обновления при сохранении
function watchFiles(params) {
    gulp.watch([path.watch.html],html);
    gulp.watch([path.watch.css],css);
    gulp.watch([path.watch.js],js);
    gulp.watch([path.watch.img],images);
}
//Объявляум функцию которая чистит папку
function clean(params){
    return del(path.clean);
}
//Создаём переменные
let build = gulp.series(clean, gulp.parallel(bs, js, css, html, images, fonts), fontsStyle);
let watch = gulp.parallel(build, watchFiles, browserSync);
//Создаём переменные для gulp

exports.bs         = bs;
exports.fontsStyle = fontsStyle;
exports.fonts      = fonts;
exports.images     = images;
exports.js         = js;
exports.css        = css;
exports.html       = html;
exports.build      = build;
exports.watch      = watch;
exports.default    = watch;